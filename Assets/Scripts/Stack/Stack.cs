using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Stack : MonoBehaviour
{
    public List<CardController> cards;

    private void Awake()
    {
        cards = new List<CardController>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        if (GetComponent<RectTransform>())
        {
            RectTransform rectTransform = GetComponent<RectTransform>();

            Vector2 rectSize = new Vector2(rectTransform.sizeDelta.x * rectTransform.lossyScale.x,
    rectTransform.sizeDelta.y * rectTransform.lossyScale.y);

            Vector2 centre = new Vector2(rectTransform.position.x + ((0.5f - rectTransform.pivot.x) * rectSize.x), 
                rectTransform.position.y + ((0.5f - rectTransform.pivot.y) * rectSize.y));


            Gizmos.DrawWireCube(centre, rectSize);
        }

    }

    public virtual void AddCard(CardController card)
    {
        cards.Add(card);
    }

    public virtual void RemoveCard(CardController card)
    {
        cards.Remove(card);
    }

    public virtual int GetIndex(CardController card)
    {
        return cards.IndexOf(card);
    }

    public virtual void ClearCards()
    {
        cards.Clear();
    }

    public virtual bool HasCard(CardController card)
    {
        return cards.Contains(card);
    }

    public virtual bool Check(CardController card)
    {
        return false;
    }

    public virtual void UndoCard(CardController card, bool remove)
    {
        if (remove)
            cards.Remove(card);
        else
            cards.Add(card);
    }

    public virtual Vector3 GetCardPosition(CardController card)
    {
        return transform.position;
    }
}
