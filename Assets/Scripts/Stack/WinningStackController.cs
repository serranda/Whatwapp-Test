using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CardInfo;
using static Move;

public class WinningStackController : Stack
{
    [SerializeField] private Suits stackSuit;

    public override void AddCard(CardController card)
    {
        Move move = new Move(MoveManager.Instance.GetNextID(MoveType.CardTravel), card,
            card.CurrentStack, this, MoveType.CardTravel, MoveManager.Instance.toWinningScore, MoveManager.Instance.GetTotalScore());
        MoveManager.Instance.AddMove(move);

        cards.Add(card);

        GameManager.Instance.CheckWin();
    }

    public override bool Check(CardController card)
    {
        if (cards.Count > 0)
        {
            //stack has already some card; check if the new one can be added
            CardController lastCard = StackManager.Instance.GetLastCardOnStack(this);
            if (lastCard.card.value == card.card.value - 1 && lastCard.card.suit == card.card.suit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            //stack has no card; check if the new one is an ace and has the right suit
            if (card.card.value == 1 && card.card.suit == stackSuit)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }

}
