using UnityEngine;
using static Move;

public class BottomStackController : Stack
{
    [SerializeField] private float discoveredCardOffset;
    [SerializeField] private float coveredCardOffset;


    public override void AddCard(CardController card)
    {
        var score = 0;
        //CASE 1.1: starting stack was the discard stack bottom stack
        if (card.CurrentStack.CompareTag("DiscardStack"))
            //add 5 points
            score = MoveManager.Instance.discardToBottomScore;
        //CASE 1.2: starting stack was a winning stack
        else if (card.CurrentStack.CompareTag("WinningStack"))
            //subtract 15 points
            score = MoveManager.Instance.winningToBottomScore;

        if (card.CurrentStack != this)
        {
            Move newMove;
            if (!card.isChild)
                newMove = new Move(MoveManager.Instance.GetNextID(MoveType.CardTravel), card,
                    card.CurrentStack, this, MoveType.CardTravel, score, MoveManager.Instance.GetTotalScore());
            else
                newMove = new Move(MoveManager.Instance.GetNextID(MoveType.ChildTravel), card,
                    card.CurrentStack, this, MoveType.ChildTravel, score, MoveManager.Instance.GetTotalScore());

            MoveManager.Instance.AddMove(newMove);
        }

        cards.Add(card);
    }

    public override void RemoveCard(CardController card)
    {
        if (cards.Count > 1)
        {
            var cardToCheck =
                StackManager.Instance.GetCardAtFromStack(this, StackManager.Instance.GetLastCardIndexOnStack(this) - 1);
            //check if need to show last card
            if (!cardToCheck.IsVisible)
            {
                //crad needs to be shown
                cardToCheck.IsMovable = true;
                cardToCheck.RotateCard(new Vector3(0, 90, 0), Vector3.zero, cardToCheck.rotationTime, true);

                //create the Move related to the discored card
                var newMove = new Move(MoveManager.Instance.GetNextID(MoveType.CardDiscover), cardToCheck,
                    this, this, MoveType.CardDiscover, MoveManager.Instance.discoveredCardScore,
                    MoveManager.Instance.GetTotalScore());
                MoveManager.Instance.AddMove(newMove);
            }
        }

        cards.Remove(card);
    }

    public override bool Check(CardController card)
    {
        if (cards.Count > 0)
        {
            //stack has already some card; check if the new one can be added
            var lastCard = StackManager.Instance.GetLastCardOnStack(this);
            if (lastCard.card.value == card.card.value + 1 && lastCard.card.color != card.card.color)
                return true;
            return false;
        }

        //stack has no card; check if the new one is a king
        if (card.card.value == 13)
            return true;
        return false;
    }

    public override Vector3 GetCardPosition(CardController card)
    {
        //2 case;
        //need to find position after adding a card (card not in list)
        //need to find positionwhen restoring card position (card already in list)

        //position is taken always after addinmg cart to the stack
        if (StackManager.Instance.GetStackSize(this) == 1) return transform.position;

        var discoveredCards = cards.FindAll(x => x.IsVisible && x.card != card.card).Count;
        var coveredCards = cards.FindAll(x => !x.IsVisible && x.card != card.card).Count;

        var offset = discoveredCards * discoveredCardOffset + coveredCards * coveredCardOffset;

        return transform.position + Vector3.down * offset;
    }
}