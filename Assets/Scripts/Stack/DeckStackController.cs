using UnityEngine.EventSystems;
using static Move;

public class DeckStackController : Stack, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        //serve a new card
        if (cards.Count > 0)
        {
            if (SettingManager.Instance.GetDraw3())
            {
                for (var i = 0; i < 3; i++)
                    if (cards.Count > 0)
                    {
                        var newCard = cards[0];

                        if (i == 2)
                        {
                            var newMove = new Move(MoveManager.Instance.GetNextID(MoveType.CardTravel), newCard,
                                this, StackManager.Instance.discardStack, MoveType.CardTravel, 0,
                                MoveManager.Instance.GetTotalScore());
                            MoveManager.Instance.AddMove(newMove);
                        }
                        else
                        {
                            var newMove = new Move(MoveManager.Instance.GetNextID(MoveType.Draw3Travel), newCard,
                                this, StackManager.Instance.discardStack, MoveType.Draw3Travel, 0,
                                MoveManager.Instance.GetTotalScore());
                            MoveManager.Instance.AddMove(newMove);
                        }

                        StackManager.Instance.MoveCardBetweenStacks(this, StackManager.Instance.discardStack,
                            newCard, true, true, true, true);
                    }
            }
            else
            {
                var newCard = cards[0];

                var newMove = new Move(MoveManager.Instance.GetNextID(MoveType.CardTravel), newCard,
                    this, StackManager.Instance.discardStack, MoveType.CardTravel, 0,
                    MoveManager.Instance.GetTotalScore());
                MoveManager.Instance.AddMove(newMove);

                StackManager.Instance.MoveCardBetweenStacks(this, StackManager.Instance.discardStack,
                    newCard, true, true, true, true);
            }
        }
        else if (cards.Count == 0)
        {
            //RESET CARD

            var stackSize = StackManager.Instance.GetStackSize(StackManager.Instance.discardStack);

            for (var i = 0; i < stackSize; i++)
            {
                var card = StackManager.Instance.GetCardAtFromStack(StackManager.Instance.discardStack, 0);

                var newScore = 0;

                if (i == stackSize - 1) newScore = MoveManager.Instance.resetDeckScore;

                var newMove = new Move(MoveManager.Instance.GetNextID(MoveType.DeckReset), card,
                    StackManager.Instance.discardStack, this, MoveType.DeckReset, newScore,
                    MoveManager.Instance.GetTotalScore());
                MoveManager.Instance.AddMove(newMove);

                StackManager.Instance.MoveCardBetweenStacks(StackManager.Instance.discardStack,
                    StackManager.Instance.deckStack, card, true, true, false, false);
            }
        }
    }

    public override void AddCard(CardController card)
    {
        //move card and hide it
        card.IsMovable = false;

        cards.Add(card);
    }

    public override void UndoCard(CardController card, bool remove)
    {
        if (remove)
            cards.Remove(card);
        else
            cards.Insert(0, card);
    }
}