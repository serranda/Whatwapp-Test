using UnityEngine;

public class DiscardStackController : Stack
{
    [SerializeField] private float cardDistance;


    public override void AddCard(CardController card)
    {
        var firstMovingIndex = StackManager.Instance.GetLastCardIndexOnStack(this);

        cards.Add(card);

        //move the last two cards away
        for (var i = firstMovingIndex; i > firstMovingIndex - 2 && i >= 0; i--)
        {
            cards[i].IsMovable = false;
            var position = StackManager.Instance.GetCardPositionOnStack(this, cards[i]);
            cards[i].MoveCardTo(position, cards[i].movementTime);
        }
    }

    public override void RemoveCard(CardController card)
    {
        var firstMovingIndex = StackManager.Instance.GetLastCardIndexOnStack(this) - 1;
        //move the last two card on top position
        cards.Remove(card);

        for (var i = firstMovingIndex; i > firstMovingIndex - 2 && i >= 0; i--)
        {
            cards[i].IsMovable = i == firstMovingIndex;
            var position = StackManager.Instance.GetCardPositionOnStack(this, cards[i]);
            cards[i].MoveCardTo(position, cards[i].movementTime);
        }
    }

    public override Vector3 GetCardPosition(CardController card)
    {
        Vector3 position;
        if (StackManager.Instance.GetStackSize(this) > 3)
            switch (StackManager.Instance.GetLastCardIndexOnStack(this) -
                    StackManager.Instance.GetCardIndexOnStack(this, card))
            {
                case 0:
                    position = transform.position;
                    break;
                case 1:
                    position = transform.position + Vector3.left * cardDistance;
                    break;
                default:
                    position = transform.position + Vector3.left * cardDistance * 2;
                    break;
            }
        else
            switch (StackManager.Instance.GetCardIndexOnStack(this, card))
            {
                case 0:
                    position = transform.position + Vector3.left * cardDistance * 2;
                    break;
                case 1:
                    position = transform.position + Vector3.left * cardDistance;
                    break;
                default:
                    position = transform.position;
                    break;
            }

        return position;
    }

    public override void UndoCard(CardController card, bool remove)
    {
        if (remove)
        {
            var firstMovingIndex = StackManager.Instance.GetLastCardIndexOnStack(this) - 1;
            //move the last two card on top position
            cards.Remove(card);

            for (var i = firstMovingIndex; i > firstMovingIndex - 2 && i >= 0; i--)
            {
                cards[i].IsMovable = i == firstMovingIndex;
                var position = StackManager.Instance.GetCardPositionOnStack(this, cards[i]);
                cards[i].MoveCardTo(position, cards[i].movementTime);
            }
        }
        else
        {
            var firstMovingIndex = StackManager.Instance.GetLastCardIndexOnStack(this);

            cards.Add(card);

            //move the last two cards away
            for (var i = firstMovingIndex; i > firstMovingIndex - 2 && i >= 0; i--)
            {
                cards[i].IsMovable = false;
                var position = StackManager.Instance.GetCardPositionOnStack(this, cards[i]);
                cards[i].MoveCardTo(position, cards[i].movementTime);
            }
        }
    }
}