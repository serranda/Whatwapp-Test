using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    //reference for score and move field to update
    [Header("TOP UI TEXT")]
    [SerializeField] private TextMeshProUGUI scoreUI;
    [SerializeField] private TextMeshProUGUI moveUI;

    [Header("BOTTOM UI BUTTON")]
    [SerializeField] private Button undoButton;
    [SerializeField] private Button hintButton;

    [Header("POP UP PANEL")]
    [SerializeField] private GameObject victoryPanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject newGamePanel;

    [Header("SETTINGS REFERENCE")]
    [SerializeField] private Toggle draw3Toggle;
    [SerializeField] private Toggle hintToggle;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private GameObject newGameAlert;

    private bool startingDraw3;
    private bool startingHint;

    private void Start()
    {
        scoreUI.text = "0";
        moveUI.text = "0";

        victoryPanel.SetActive(false);
    }

    private void Update()
    {
        draw3Toggle.isOn = SettingManager.Instance.GetDraw3();
        hintToggle.isOn = SettingManager.Instance.GetHintEnabled();

        newGameAlert.SetActive(CheckNewSettings());
    }

    public void SetScoreAmount(int amount)
    {
        scoreUI.text = amount.ToString();
    }

    public void SetMoveAmount(int amount)
    {
        moveUI.text = amount.ToString();
    }

    public void SetUndoButton(bool enabled)
    {
        undoButton.interactable = enabled;
    }

    public void SetHintButton(bool enabled)
    {
        hintButton.interactable = enabled;
    }

    public void NewGame(bool shuffle)
    {
        GameManager.Instance.NewGame(shuffle);
    }

    public void GetHint()
    {
        GameManager.Instance.GetHint();
    }

    public void ShowVictoryPanel()
    {
        victoryPanel.SetActive(true);
    }

    public void ShowSettingPanel()
    {
        settingsPanel.SetActive(true);
        startingDraw3 = SettingManager.Instance.GetDraw3();
        startingHint = SettingManager.Instance.GetHintEnabled();
    }

    public void ToggleNewGamePanel(bool active)
    {
        newGamePanel.SetActive(active);
    }

    public void Draw3Toggle()
    {
        SettingManager.Instance.SetDraw3(draw3Toggle.isOn);
    }

    public void HintToggle()
    {
        SettingManager.Instance.SetHintEnabled(hintToggle.isOn);
    }

    public void ApplyNewSettings()
    {
        settingsPanel.SetActive(false);

        if (CheckNewSettings())
        {
            GameManager.Instance.NewGame(true);
        }
    }

    public void ChangeBackgroundColor(Image image)
    {
        //background.color = image.color;
        mainCamera.backgroundColor = image.color;

    }

    private bool CheckNewSettings()
    {
        if (startingDraw3 != SettingManager.Instance.GetDraw3() ||
            startingHint != SettingManager.Instance.GetHintEnabled())
            return true;
        else
            return false;
    }

}
