using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingManager : Singleton<SettingManager>
{
    private bool isDraw3;
    
    private bool hintEnabled;

    protected override void Awake()
    {
        base.Awake();

        if (PlayerPrefs.HasKey("isDraw"))
            isDraw3 = bool.Parse(PlayerPrefs.GetString("isDraw"));
        if (PlayerPrefs.HasKey("hintEnabled"))
            hintEnabled = bool.Parse(PlayerPrefs.GetString("hintEnabled"));
    }

    public void SetDraw3(bool draw3)
    {
        isDraw3 = draw3;
        PlayerPrefs.SetString("isDraw", isDraw3.ToString());
        PlayerPrefs.Save();
    }

    public bool GetDraw3()
    {
        return isDraw3;
    }

    public void SetHintEnabled(bool hint)
    {
        hintEnabled = hint;
        PlayerPrefs.SetString("hintEnabled", hintEnabled.ToString());
        PlayerPrefs.Save();
    }

    public bool GetHintEnabled()
    {
        return hintEnabled;
    }

}
