using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SolitaireAction;


public class InputManager : Singleton<InputManager>
{
    private SolitaireAction actions;
    private PlayerActions playerActions;
    private UIActions uiActions;

    protected override void Awake()
    {
        actions = new SolitaireAction();

        playerActions = actions.Player;
        uiActions = actions.UI;

        actions.Enable();
        base.Awake();
    }

    private void OnDisable()
    {
        actions.Disable();
    }

    public void SetPlayerAction(bool enabled)
    {
        if (enabled)
        {
            playerActions.Enable();
        }
        else
        {
            playerActions.Disable();
        }
    }

    public PlayerActions GetPlayerActions()
    {
        return playerActions;
    }

    public void SetUIAction(bool enabled)
    {
        if (enabled)
        {
            uiActions.Enable();
        }
        else
        {
            uiActions.Disable();
        }
    }
}