using UnityEngine;

public class StackManager : Singleton<StackManager>
{
    [Header("TABLE STACKS")] public Stack[] bottomStacks;

    public Stack[] winningStacks;
    public Stack deckStack;
    public Stack discardStack;

    public void AddCardToStack(Stack stack, CardController card)
    {
        stack.AddCard(card);
        card.CurrentStack = stack;
        card.transform.SetParent(stack.transform);
        card.OrderInLayer = GetCardIndexOnStack(stack, card) + 1;
    }

    public void RemoveCardFromStack(Stack stack, CardController card)
    {
        if (stack.HasCard(card)) stack.RemoveCard(card);
    }

    public void MoveCardBetweenStacks(Stack fromStack, Stack toStack, CardController card, bool move, bool rotate,
        bool cardVisible, bool cardMovable)
    {
        //add to new stack adn set parent
        AddCardToStack(toStack, card);

        //remove from old stack
        RemoveCardFromStack(fromStack, card);

        //manage the card movement and/or rotation
        if (move)
        {
            var position = GetCardPositionOnStack(toStack, card);
            card.MoveCardTo(position, card.movementTime);
        }

        if (rotate)
            card.RotateCard(new Vector3(0, -90, 0), Vector3.zero, card.rotationTime, cardVisible);
        card.IsMovable = cardMovable;
    }

    public int GetCardIndexOnStack(Stack stack, CardController card)
    {
        return stack.HasCard(card) ? stack.GetIndex(card) : -1;
    }

    public CardController GetCardAtFromStack(Stack stack, int index)
    {
        return stack.cards[index];
    }

    public void ClearStack(Stack stack)
    {
        stack.ClearCards();
    }

    public bool CheckCardOnStack(Stack stack, CardController card)
    {
        return stack.Check(card);
    }

    public bool IsLastCardOnStack(Stack stack, CardController card)
    {
        return stack.GetIndex(card) == stack.cards.Count - 1;
    }

    public int GetLastCardIndexOnStack(Stack stack)
    {
        return stack.cards.Count - 1;
    }

    public CardController GetLastCardOnStack(Stack stack)
    {
        return stack.cards[stack.cards.Count - 1];
    }

    public int GetStackSize(Stack stack)
    {
        return stack.cards.Count;
    }

    public Vector3 GetCardPositionOnStack(Stack stack, CardController card)
    {
        return stack.GetCardPosition(card);
    }

    public void RestoreCardOnStackAferUndo(Stack fromStack, Stack toStack, CardController card, bool move = true,
        bool rotate = true, bool cardVisible = true, bool cardMovable = true)
    {
        //add to new stack adn set parent
        toStack.UndoCard(card, false);
        card.CurrentStack = toStack;
        card.transform.SetParent(toStack.transform);
        card.OrderInLayer = GetCardIndexOnStack(toStack, card) + 1;

        //remove from old stack
        fromStack.UndoCard(card, true);

        //manage the card movement and/or rotation
        if (move)
        {
            var position = GetCardPositionOnStack(toStack, card);
            card.MoveCardTo(position, card.movementTime);
        }

        if (rotate)
            card.RotateCard(new Vector3(0, -90, 0), Vector3.zero, card.rotationTime, cardVisible);
        card.IsMovable = cardMovable;
    }

    public void ClearAllStacks()
    {
        //clear all stacks to start a new game
        for (var i = 0; i < winningStacks.Length; i++) ClearStack(winningStacks[i]);

        for (var i = 0; i < bottomStacks.Length; i++) ClearStack(bottomStacks[i]);

        ClearStack(deckStack);
        ClearStack(discardStack);
    }
}