using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.ScriptableObject;
using Random = System.Random;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private Deck deckConfig;

    [SerializeField] private bool disableDebug;

    private readonly List<CardController> deck = new List<CardController>();

    private List<CardController> movableDeck = new List<CardController>();

    protected override void Awake()
    {
        base.Awake();

        Debug.unityLogger.logEnabled = !disableDebug;
    }

    private void Start()
    {
        //create card GO from SO
        InstantiateDeck(deckConfig);

        //start a new game
        NewGame(true);
    }

    public void NewGame(bool shuffle)
    {
        //stop all coroutine (needed if new game is called before the serve coroutine is finished)
        StopAllCoroutines();

        //disable input (will be enable on serve ending
        InputManager.Instance.SetPlayerAction(false);

        //Clear all moves
        MoveManager.Instance.ClearMoves();

        //clear all stacks from previous assigned card
        StackManager.Instance.ClearAllStacks();

        UIManager.Instance.SetHintButton(SettingManager.Instance.GetHintEnabled());

        if (shuffle)
            //Shuffle card
            Shuffle(deck);

        //Set te card in position
        MoveToDeckController(deck);

        //serve a new table
        NewTable(deck);
    }

    private void InstantiateDeck(Deck newDeck)
    {
        foreach (var suitSprite in newDeck.suits)
        {
            var newSuit = (CardInfo.Suits) Enum.Parse(typeof(CardInfo.Suits), suitSprite.name);
            var newColor = (int) newSuit % 2 == 0 ? newDeck.redColor : newDeck.blackColor;

            foreach (var valueSprite in newDeck.values)
            {
                var newCard = CreateInstance<Card>();

                newCard.value = int.Parse(valueSprite.name);
                newCard.suit = newSuit;
                newCard.color = newColor;

                newCard.suitSprite = suitSprite;
                newCard.valueSprite = valueSprite;
                newCard.frontSprite = newDeck.cardFront;
                newCard.backSprite = newDeck.cardBack;

                //instantiate new card GO
                var newCardController = Instantiate(newDeck.cardPrefab,
                    StackManager.Instance.deckStack.transform.position,
                    Quaternion.identity, StackManager.Instance.deckStack.transform);

                //set card info and sprite; remove card from deck
                newCardController.SetNewCardSprites(newCard);
                deck.Add(newCardController);
            }
        }
    }

    private void Shuffle(List<CardController> cards)
    {
        //shuffle card
        for (var i = 0; i < 3; i++)
        {
            var rng = new Random();

            var n = cards.Count;

            while (n > 1)
            {
                n--;
                var k = rng.Next(n + 1);
                (cards[k], cards[n]) = (cards[n], cards[k]);
            }
        }
    }

    public void CheckWin()
    {
        var win = StackManager.Instance.winningStacks.Aggregate(true,
            (current, stack) => current & (StackManager.Instance.GetStackSize(stack) == 13));

        if (win) UIManager.Instance.ShowVictoryPanel();
    }

    private void MoveToDeckController(List<CardController> cards)
    {
        foreach (var card in cards)
        {
            //move card to deck
            card.transform.position = StackManager.Instance.deckStack.transform.position;
            card.ClearCardInfo();

            //add card to the serving stack
            StackManager.Instance.AddCardToStack(StackManager.Instance.deckStack, card);
        }
    }

    private void NewTable(List<CardController> cards)
    {
        StartCoroutine(CreateTable(cards));
    }

    private IEnumerator CreateTable(List<CardController> cards)
    {
        yield return new WaitForSeconds(1f);

        var k = 0;
        //serve card in table
        for (var i = 0; i < StackManager.Instance.bottomStacks.Length; i++)
        for (var j = 0; j < i + 1; j++)
        {
            if (j == i)
                StackManager.Instance.MoveCardBetweenStacks(StackManager.Instance.deckStack,
                    StackManager.Instance.bottomStacks[i], cards[k], true, true, true, true);
            else
                //move card to position 
                StackManager.Instance.MoveCardBetweenStacks(StackManager.Instance.deckStack,
                    StackManager.Instance.bottomStacks[i], cards[k], true, false, false, false);

            k++;

            yield return new WaitForSeconds(0.1f);
        }

        //Enable Input
        InputManager.Instance.SetPlayerAction(true);
    }

    public void GetHint()
    {
        //Get a hint about a possible next move; the hint is given in order of a given hierarchy
        //first check if the winning stack can receive a card
        //second check if the bottom stack can receive a card
        // the checks are made first on the discard stack and then the bottom stack
        //THE HIERARCHY IS
        //winning stack from discard
        //winning stack from bottom
        //bottom from discard
        //bottom from bottom N.B if a bottom card which can be move is already attached to a similar one, the move is useless so it won't be hinted

        movableDeck = deck.FindAll(x => x.IsMovable && !x.CurrentStack.CompareTag("WinningStack"))
            .ToList();

        //reorder list to get the discard stack card on top
        var index = movableDeck.FindIndex(x => x.CurrentStack.CompareTag("DiscardStack"));
        if (index >= 0)
        {
            var cardToMove = movableDeck[index];
            movableDeck.RemoveAt(index);
            movableDeck.Insert(0, cardToMove);
        }

        foreach (var card in movableDeck)

        foreach (var stack in StackManager.Instance.winningStacks)
            if (StackManager.Instance.CheckCardOnStack(stack, card))
            {
                Debug.Log("NEW MOVE POSSIBLE ON CARD " + card.card);
                card.HintCard(stack);
                return;
            }

        foreach (var card in movableDeck)
        foreach (var stack in StackManager.Instance.bottomStacks)
            if (StackManager.Instance.CheckCardOnStack(stack, card))
            {
                //get previous card to see if the card move is useful or not
                var previousCardIndex =
                    StackManager.Instance.GetCardIndexOnStack(card.CurrentStack, card) - 1;


                //if previousCardIndex < 0 means that the card is a K e and moving to a new position in the bottom zone wouldn't mean any advantage
                if (previousCardIndex < 0) continue;

                var previousCard =
                    StackManager.Instance.GetCardAtFromStack(card.CurrentStack,
                        previousCardIndex);

                //if previousCard is movable, means that the current card is already attached under a card and move it wouldn't mean any advantage
                if (previousCard.IsMovable) continue;

                Debug.Log("NEW MOVE POSSIBLE ON CARD " + card.card);
                card.HintCard(stack);
                return;
            }
    }
}