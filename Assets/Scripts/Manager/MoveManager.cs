using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using static Move;

public class MoveManager : Singleton<MoveManager>
{
    private List<Move> moves = new List<Move>();

    [Header("MOVE SCORE VALUES")]
    public int discardToBottomScore = 5;
    public int winningToBottomScore = -15;
    public int discoveredCardScore = 5;
    public int toWinningScore = 10;
    public int resetDeckScore = -100;

    private int totalScore;

    private int nextUndoMoveId;

    private void Start()
    {
        UIManager.Instance.SetUndoButton(false);
    }

    public int GetTotalScore()
    {
        return totalScore;
    }

    public void AddMove(Move newMove)
    {
        //add new move to the list
        moves.Add(newMove);

        //update score
        totalScore += newMove.score;

        if (totalScore < 0)
            totalScore = 0;

        //update UI
        UIManager.Instance.SetMoveAmount(newMove.id);
        UIManager.Instance.SetScoreAmount(totalScore);

        //get next undomoveid
        nextUndoMoveId = GetNextUndoMoveId();
    }

    //called when creating a new game
    //reset all ifno
    public void ClearMoves()
    {
        //reset id for next uno move
        nextUndoMoveId = 0;

        //reset total score
        totalScore = 0;

        //clear move list
        moves.Clear();

        //reset UI
        UIManager.Instance.SetMoveAmount(0);
        UIManager.Instance.SetScoreAmount(0);
    }

    public void UndoLastMoveId()
    {
        if (moves.Count > 0 && nextUndoMoveId != 0)
        {
            //get new moive id before undoing moves
            int newMoveID = GetNextID(MoveType.Undo);

            //used for reset score later
            int scoreToRestore = 0;

            //get list of last move by id
            List<Move> lastMoves = moves.FindAll(x => x.id == nextUndoMoveId);
            lastMoves.Sort((x, y) => x.type.CompareTo(y.type));
            for (int i = 0; i < lastMoves.Count; i++)
            {
                //check what move has been made to reproduce the opposite card motion
                switch (lastMoves[i].type)
                {
                    //move was a deck reset; switch back deck and discard stack
                    case MoveType.DeckReset:
                        int stackSize = StackManager.Instance.GetStackSize(StackManager.Instance.deckStack);

                        for (int j = 0; j < stackSize; j++)
                        {
                            CardController card = StackManager.Instance.GetCardAtFromStack(StackManager.Instance.deckStack, 0);

                            StackManager.Instance.RestoreCardOnStackAferUndo(lastMoves[i].endingStack,
                                lastMoves[i].startingStack, card, true, true, true, j == stackSize - 1);
                        }
                        break;
                    //move was a card discover; rotate card and disable it
                    case MoveType.CardDiscover:
                        //crad needs to be shown

                        StackManager.Instance.RestoreCardOnStackAferUndo(lastMoves[i].endingStack, lastMoves[i].startingStack,
                            lastMoves[i].card, false, true, false, false);
                        break;
                    //move was a card tarvel; send back card to previous stack
                    case MoveType.CardTravel:
                        //card has been moved from deck; it's a new card that has to go back to deck; hide it
                        if (lastMoves[i].startingStack == StackManager.Instance.deckStack)
                            StackManager.Instance.RestoreCardOnStackAferUndo(lastMoves[i].endingStack, lastMoves[i].startingStack,
                                lastMoves[i].card, true, true, false, false);
                        else
                            //card has been moved from the other stack and can be remain visible and movable
                            StackManager.Instance.RestoreCardOnStackAferUndo(lastMoves[i].endingStack, lastMoves[i].startingStack,
                                lastMoves[i].card, true, false, true, true);
                        break;
                    case MoveType.Draw3Travel:
                        StackManager.Instance.RestoreCardOnStackAferUndo(lastMoves[i].endingStack, lastMoves[i].startingStack,
                            lastMoves[i].card, true, true, false, false);
                        break;
                    case MoveType.ChildTravel:
                        StackManager.Instance.RestoreCardOnStackAferUndo(lastMoves[i].endingStack, lastMoves[i].startingStack,
                            lastMoves[i].card, true, false, true, true);
                        break;
                    default:
                        Debug.LogError("ERROR UNDOING MOVE " + lastMoves[i].ToString());
                        break;
                }


                scoreToRestore = lastMoves[i].currentTotalScore;
                Debug.Log("UNDO " + lastMoves[i]);

                moves.Remove(lastMoves[i]);
            }

            //update score
            totalScore = scoreToRestore;

            //Create new undo move
            Move newMove = new Move(newMoveID, MoveType.Undo);
            AddMove(newMove);

            //override Ui to set the right score
            UIManager.Instance.SetScoreAmount(scoreToRestore);
        }
        else
        {
            Debug.Log("NO MOVE TO UNDO");
        }

    }

    public int GetNextID(MoveType type)
    {
        int newID;

        //check if there are other move in memory
        //CASE 1 other move already made; get new id
        if (moves.Count > 0)
        {
            Move lastMove = moves[moves.Count - 1];
            //a card discover happen always after a card travel
            //if type is carddiscover copy previous id 

            if (type == MoveType.CardDiscover ||
                type == MoveType.ChildTravel)
            {
                newID = lastMove.id;
            }
            else
            {
                //if draw 3 when moving a card to discard stack the move id doesn't need to increase
                if (lastMove.type == MoveType.CardTravel ||
                    lastMove.type == MoveType.Undo ||
                    lastMove.type == MoveType.CardDiscover ||
                    (lastMove.type == MoveType.DeckReset && lastMove.score != 0))
                {
                    newID = lastMove.id + 1;
                }
                else
                {
                    newID = lastMove.id;
                }
            }
        }
        //CASE 2 first move id = 1;
        else
        {
            newID = 1;
        }

        return newID;
    }

    private int GetLastID()
    {
        return moves[moves.Count - 1].id;
    }

    private int GetNextUndoMoveId()
    {
        Move lastUndoMove = moves.FindLast(x => x.type != MoveType.Undo && x.startingStack != x.endingStack);
        if (lastUndoMove != null)
        {
            //update UI
            UIManager.Instance.SetUndoButton(true);

            return lastUndoMove.id;

        }
        else
        {
            //update UI
            UIManager.Instance.SetUndoButton(false);

            return 0;
        }
    }
}
