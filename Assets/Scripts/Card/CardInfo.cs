using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CardInfo
{
    public enum Suits
    {
        Hearts,
        Clubs,
        Diamonds,
        Spades
    };

    public enum Colors
    {
        Red,
        Black
    };
}