using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using static SolitaireAction;

public class CardController : MonoBehaviour
{
    [Header("CARD SPRITE LAYER")] [SerializeField]
    private string cardSortingLayer;

    [SerializeField] private string movingSortingLayer;
    [SerializeField] private SortingGroup sortingGroup;

    [Header("CARD SPRITE RENDERER")] [SerializeField]
    private SpriteRenderer valueRenderer;

    [SerializeField] private SpriteRenderer topSuitRenderer;
    [SerializeField] private SpriteRenderer middleSuitRenderer;
    [SerializeField] private SpriteRenderer backRenderer;
    [SerializeField] private SpriteRenderer frontRenderer;

    [Header("CARD COLLIDER INFO")] [SerializeField]
    private BoxCollider2D boxCollider2D;

    [SerializeField] private LayerMask collisionMask;

    [Header("ANIMATION INFO")] public float movementTime;
    public float rotationTime;
    public float followTime;


    [HideInInspector] public Card card;
    [HideInInspector] public bool isChild;

    private List<CardController> childrenCard = new List<CardController>();

    private bool followMouse;

    private Camera mainCamera;

    private Vector3 mouseOffset;

    private Tween moveTween;

    private PlayerActions playerActions;
    private Sequence rotateSequence;

    private Vector3 startingPosition;

    public Stack CurrentStack { get; set; }

    public bool IsVisible { get; private set; }

    public bool IsMovable
    {
        get => boxCollider2D.enabled;
        set => boxCollider2D.enabled = value;
    }

    private string SortingLayer
    {
        set => sortingGroup.sortingLayerName = value;
    }

    public int OrderInLayer
    {
        set
        {
            sortingGroup.sortingOrder = value;
            transform.position = new Vector3(transform.position.x, transform.position.y, -value);
        }
    }

    private float Transparency
    {
        set
        {
            valueRenderer.color = Color.white * value;
            topSuitRenderer.color = Color.white * value;
            middleSuitRenderer.color = Color.white * value;
            backRenderer.color = Color.white * value;
        }
    }


    private void Awake()
    {
        //set default sorting layer
        SortingLayer = cardSortingLayer;
        IsVisible = false;
        IsMovable = false;

        playerActions = InputManager.Instance.GetPlayerActions();

        playerActions.Drag.started += OnBeginDrag;
        playerActions.Drag.canceled += OnEndDrag;

        playerActions.DoubleClick.performed += OnDoubleClick;

        if (Camera.main == null) return;

        mainCamera = Camera.main;
    }

    private void FixedUpdate()
    {
        if (!followMouse) return;
        //get newPosition from mouse

        var newPosition = mainCamera.ScreenToWorldPoint(Mouse.current.position.ReadValue()) + mouseOffset;

        //follow mouse
        MoveCardTo(newPosition, movementTime);

        if (childrenCard.Count <= 0) return;

        //card selected is not the last
        //move also the other card
        foreach (var child in childrenCard)
        {
            var i = childrenCard.IndexOf(child);
            child.MoveCardTo(newPosition + Vector3.down * (0.4f * (i + 1)),
                child.movementTime + child.followTime * (i + 1));
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawLine(Mouse.current.position.ReadValue(),
            (Vector3) Mouse.current.position.ReadValue() + Vector3.forward * 1000);
    }

    private void OnDoubleClick(InputAction.CallbackContext context)
    {
        if (!CheckClickOnCard()) return;

        //check in every stack if there is a different place for the card
        foreach (var stack in StackManager.Instance.winningStacks)
        {
            if (!StackManager.Instance.CheckCardOnStack(stack, this) ||
                !StackManager.Instance.IsLastCardOnStack(CurrentStack, this)) continue;

            SortingLayer = movingSortingLayer;

            StackManager.Instance.MoveCardBetweenStacks(CurrentStack,
                stack, this, true, false, true, true);

            SortingLayer = cardSortingLayer;

            return;
        }

        foreach (var stack in StackManager.Instance.bottomStacks)
        {
            if (!StackManager.Instance.CheckCardOnStack(stack, this)) continue;

            if (!StackManager.Instance.IsLastCardOnStack(CurrentStack, this)) childrenCard = GetChildrenCards();

            SortingLayer = movingSortingLayer;

            StackManager.Instance.MoveCardBetweenStacks(CurrentStack,
                stack, this, true, false, true, true);

            SortingLayer = cardSortingLayer;
            //card has other ones below it; move also the other cards
            if (childrenCard.Count <= 0) continue;

            //card selected is not the last
            //move also the other card
            foreach (var child in childrenCard)
            {
                child.SortingLayer = movingSortingLayer;

                StackManager.Instance.MoveCardBetweenStacks(child.CurrentStack,
                    CurrentStack, child, true, false, true, true);

                child.SortingLayer = cardSortingLayer;
            }

            ClearChildrenCards();
        }
    }

    private void OnBeginDrag(InputAction.CallbackContext context)
    {
        if (!CheckClickOnCard()) return;

        followMouse = true;

        startingPosition = transform.position;

        mouseOffset = startingPosition - mainCamera.ScreenToWorldPoint(Mouse.current.position.ReadValue());

        //set sorting layer to keep show card
        SortingLayer = movingSortingLayer;

        //save starting position for later

        if (StackManager.Instance.IsLastCardOnStack(CurrentStack, this)) return;
        //card selected is not the last
        childrenCard = GetChildrenCards();

        if (childrenCard.Count <= 0) return;
        foreach (var child in childrenCard)
        {
            child.SortingLayer = movingSortingLayer;
            child.startingPosition = child.transform.position;
        }
    }

    private void OnEndDrag(InputAction.CallbackContext context)
    {
        if (!followMouse) return;

        followMouse = false;

        //check if new position can be assigned
        var moved = CheckNewPosition();

        if (childrenCard.Count > 0)
        {
            //card selected is not the last
            //move also the other card
            foreach (var childCard in childrenCard)
            {
                if (moved)
                    //if card are move to a new stack, add to the new stack the current card
                    StackManager.Instance.MoveCardBetweenStacks(childCard.CurrentStack,
                        CurrentStack, childCard, true, false, true, true);
                else
                    childCard.MoveCardTo(childCard.startingPosition, childCard.movementTime);

                childCard.SortingLayer = cardSortingLayer;
            }

            ClearChildrenCards();
        }

        //set sorting layer to previous value
        SortingLayer = cardSortingLayer;
    }

    private bool CheckClickOnCard()
    {
        return boxCollider2D ==
               Physics2D.OverlapPoint(mainCamera.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
    }

    public void SetNewCardSprites(Card newCard)
    {
        card = newCard;
        valueRenderer.sprite = card.valueSprite;
        valueRenderer.color = card.color;
        topSuitRenderer.sprite = card.suitSprite;
        middleSuitRenderer.sprite = card.suitSprite;
        backRenderer.sprite = card.backSprite;
        frontRenderer.sprite = card.frontSprite;
    }

    public void RotateCard(Vector3 midAngle, Vector3 finalAngle, float elapsedTime, bool visibility)
    {
        IsVisible = visibility;

        rotateSequence = DOTween.Sequence();

        rotateSequence.Append(
                transform.DORotate(midAngle, elapsedTime).SetEase(Ease.InOutCirc)
                    .OnComplete(() => backRenderer.enabled = !visibility))
            .Append(
                transform.DORotate(finalAngle, elapsedTime).SetEase(Ease.InOutCirc));
    }

    public void MoveCardTo(Vector3 to, float elapsedTime)
    {
        //override to vector so it maintains the z position
        to = new Vector3(to.x, to.y, transform.position.z);

        //set new position to card
        if (moveTween.IsActive()) moveTween.Kill();

        moveTween = transform.DOMove(to, elapsedTime);
    }

    private bool CheckNewPosition()
    {
        var colliders = new Collider2D[2];
        var size = Physics2D.OverlapBoxNonAlloc(transform.position, boxCollider2D.size, 0, colliders,
            collisionMask);

        //new stack area entered
        if (size > 0)
        {
            //get closest stack
            var overlappingCollider = colliders[0];
            for (var i = 1; i < colliders.Length; i++)
                if (boxCollider2D.Distance(colliders[i]).distance <
                    boxCollider2D.Distance(overlappingCollider).distance)
                    overlappingCollider = colliders[i];

            //check if card can be attached to last card stack
            var stack = overlappingCollider.GetComponent<Stack>();

            if (StackManager.Instance.CheckCardOnStack(stack, this))
            {
                //Debug.Log("Moving " + card.name);

                StackManager.Instance.MoveCardBetweenStacks(CurrentStack, stack, this, true, false, true, true);
                return true;
            }

            //Debug.Log("Resetting " + card.name);
            //StackManager.Instance.MoveCardBetweenStacks(currentStack, currentStack, this, true, false, true, true);
            MoveCardTo(startingPosition, movementTime);
            return false;
        }

        //Debug.Log("No collider overlapping " + card.name);
        //StackManager.Instance.MoveCardBetweenStacks(currentStack, currentStack, this, true, false, true, true);
        MoveCardTo(startingPosition, movementTime);
        return false;
    }

    private List<CardController> GetChildrenCards()
    {
        var children = new List<CardController>();
        var startingIndex = StackManager.Instance.GetCardIndexOnStack(CurrentStack, this) + 1;
        var endingIndex = StackManager.Instance.GetLastCardIndexOnStack(CurrentStack);

        //move also the other card
        for (var i = startingIndex; i <= endingIndex; i++)
        {
            CurrentStack.cards[i].isChild = true;
            children.Add(CurrentStack.cards[i]);
        }

        return children;
    }

    private void ClearChildrenCards()
    {
        foreach (var child in childrenCard)
            child.isChild = false;

        childrenCard.Clear();
    }

    public void ClearCardInfo()
    {
        //card not movable
        IsMovable = false;

        //card not visible
        IsVisible = false;

        //enable back renderer if disabled
        if (!backRenderer.enabled)
            backRenderer.enabled = true;

        //clear children card info
        if (childrenCard.Count > 0)
            childrenCard.Clear();

        //card is no child
        isChild = false;

        //reset staring position
        startingPosition = Vector3.zero;
    }

    public void HintCard(Stack stack)
    {
        var hintedCard = Instantiate(gameObject, transform.parent).GetComponent<CardController>();

        hintedCard.SortingLayer = movingSortingLayer;
        hintedCard.Transparency = 0.5f;

        var hintSequence = DOTween.Sequence();

        hintSequence.Insert(0,
                hintedCard.transform.DOMove(StackManager.Instance.GetCardPositionOnStack(stack, hintedCard), 0.5f))
            .Insert(0, hintedCard.GetComponent<CanvasGroup>().DOFade(0, 0.5f).SetEase(Ease.OutSine))
            .OnComplete(() => Destroy(hintedCard.gameObject));
    }
}