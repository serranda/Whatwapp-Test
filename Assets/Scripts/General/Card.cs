using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CardInfo;

[CreateAssetMenu(fileName = "Card", menuName = "Card")]
public class Card : ScriptableObject
{
    public int value;
    public Suits suit;
    public Color color;
    public Sprite valueSprite;
    public Sprite suitSprite;
    public Sprite frontSprite;
    public Sprite backSprite;
}