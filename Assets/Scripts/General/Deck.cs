using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Deck", menuName = "Deck")]
public class Deck : ScriptableObject
{
    [Header("SUITS")] public List<Sprite> suits;

    [Header("VALUES")] public List<Sprite> values;

    [Header("COMMON REFERENCES")] 
    public Sprite cardBack;
    public Sprite cardFront;
    public CardController cardPrefab;

    [Header("COLORS")] public Color blackColor;
    public Color redColor;
}