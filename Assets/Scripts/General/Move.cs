using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Move
{
    public enum MoveType { CardDiscover, CardTravel, ChildTravel, Draw3Travel, DeckReset, Undo }

    public int id;
    public CardController card;
    public Stack startingStack;
    public Stack endingStack;
    public MoveType type;
    public int score;
    public int currentTotalScore;

    public Move(int id, CardController card, Stack startingStack, Stack endingStack, MoveType type, int score, int currentTotalScore)
    {
        this.id = id;
        this.card = card;
        this.startingStack = startingStack;
        this.endingStack = endingStack;
        this.type = type;
        this.score = score;
        this.currentTotalScore = currentTotalScore;
    }

    public Move(int id, MoveType type)
    {
        this.id = id;
        this.card = null;
        this.startingStack = null;
        this.endingStack = null;
        this.type = type;
        this.score = 0;
        this.currentTotalScore = 0;
    }

    public override string ToString()
    {
        if (type != MoveType.Undo)
            return string.Join(" ", id, card.card.name, startingStack.name, endingStack.name, type.ToString(), score, currentTotalScore);
        else
            return string.Join(" ", id, "", "", 0, "", type.ToString(), 0, 0);
    }

}
