#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static CardInfo;


public class CardGenerator : EditorWindow
{
    private static Sprite[] numbers;
    private static Sprite[] suits;

    private static void GetNumberSprites()
    {
        numbers = Resources.LoadAll<Sprite>("Images/Cards/Numbers");
    }

    private static void GetSuitsSprites()
    {
        suits = Resources.LoadAll<Sprite>("Images/Cards/Suits");
    }

    [MenuItem("Solitaire/Generate Card SO")]
    private static void GenerateCardSO()
    {
        GetSuitsSprites();
        GetNumberSprites();

        for (int j = 0; j < suits.Length; j++)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                Card card = CreateInstance<Card>();

                card.value = int.Parse(numbers[i].name);
                card.suit = (Suits)System.Enum.Parse(typeof(Suits), suits[j].name);
                card.color = (int)card.suit % 2 == 0 ? Color.red : Color.black;

                card.valueSprite = (Sprite)numbers[i];
                card.suitSprite = (Sprite)suits[j];

                AssetDatabase.CreateAsset(card, string.Concat("Assets/Resources/Card/", card.value.ToString(), card.suit.ToString(), ".asset"));
                AssetDatabase.SaveAssets();
            }
        }

    }
}

#endif

